import {StyleSheet} from 'react-native'
export const styles = StyleSheet.create({
  mainContainer: {
    flex:1,
    backgroundColor:'white'
  },
  logoContainer:{
    flex:2,paddingTop:80,
    justifyContent:'center',
    alignItems:'center'
  },
  logo:{
    width:'60%'
  },
  inputContainer:{
    flex:3,
    padding:20
  },
  link:{
    textDecorationLine:'underline',
    textAlign:'center'
  }
});