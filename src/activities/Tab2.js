import React from 'react';
import {StyleSheet, Text, View, AsyncStorage, ScrollView} from 'react-native';
import {mainurl} from '../loginApi';
import MyPendings from './MyPendings';
import axios from 'axios';
import {Spinner} from "../components/common";

export default class Tab2 extends React.Component {

    state = {details: [], loading: true, nodata: ''};

    componentWillMount() {
        AsyncStorage.getItem('id').then((token) => {
            console.log(token);
            axios.get(mainurl + 'getMyPendings/' + token).then(response => {
                    if (response.data.pendings !== null) {
                        this.setState({details: response.data.pendings, loading: false, nodata: ''});
                    } else {
                        this.setState({nodata: 'none', loading: false});
                    }
                }
            );
        });
    }

    renderDetails() {
        if (this.state.nodata === 'none') {
            return (
                <View style={styles.fetchingData}>
                    <Text style={styles.textStyle}>Sorry, No Data Available...</Text>
                </View>
            );
        } else {
            if (this.state.loading) {
                return (
                    <View style={styles.fetchingData}>
                        <Spinner size='large'/>
                        <Text style={styles.textStyle}>Loading...</Text>
                    </View>
                );
            } else {
                return this.state.details.map(detail =>
                    <MyPendings key={detail.id} detail={detail}/>
                );
            }
        }
    }

    render() {
        return (
            <ScrollView style={{backgroundColor: '#181c28'}}>
                {this.renderDetails()}
            </ScrollView>
        );
    }
}

const styles = {
    fetchingData: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    textStyle: {
        flex: 1,
        width: null,
        color: '#fff'
    },
};
