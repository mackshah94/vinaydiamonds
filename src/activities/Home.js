import React from 'react';
import { StyleSheet, Text, TextInput,Image, View } from 'react-native';
import BottomNavigation, {
  IconTab, Badge, ShiftingTab
} from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/Feather';
import Tab1 from './Tab1';
import Tab2 from './Tab2';
import Tab3 from './Tab3';
import Tab4 from './Tab4';
import Tab5 from './Tab5';

class Home extends React.Component{

  constructor(props) {
     super(props);
     this.handleTabChange = this.handleTabChange.bind(this);
  }

  state = {
    activeTab: 'uppend'
  }

  tabs = [
    {
      key: 'uppend',
      label: 'Uploads Pending',
      barColor: '#388E3C',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'upload-cloud',
      count: '2'
    },
    {
      key: 'mypend',
      label: 'My Pendings',
      barColor: '#00695C',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'upload',
      count: '2'
    },
    {
      key: 'upload',
      label: 'Upload',
      barColor: '#6A1B9A',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'plus',
      count: '0'
    },
    {
      key: 'mypurch',
      label: 'My Purchases',
      barColor: '#1565C0',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'shopping-cart',
      count: '0'
    },
    {
      key: 'copurch',
      label: 'More...',
      barColor: '#1565C0',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'more-horizontal',
      count: '0'
    }
  ]

  state = {
    activeTab: this.tabs[0].key
  }

  renderIcon = icon => ({ isActive }) => (
    <Icon size={24} color='#fff' name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <ShiftingTab
      isActive={isActive}
      //showBadge={tab.key === 'mypend' || tab.key === 'uppend'}
      showBadge={tab.count > 0}
      renderBadge={() => <Badge>{tab.count}</Badge>}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  handleTabChange(newTabIndex, oldTabIndex) {
     this.setState({ activeTab: newTabIndex });
     if (newTabIndex === oldTabIndex) {
       null;
     }
     if (this.state.activeTab === 0) {
       this.setState({selectedTab: 'tab1',});
     } else if (this.state.activeTab === 1) {
       this.setState({selectedTab: 'tab2',});
     } else if (this.state.activeTab === 2) {
       this.setState({selectedTab: 'tab3',});
     }else if(this.state.activeTab==3){
       this.setState({selectedTab: 'tab4',});
     }
     else {
       this.setState({selectedTab: 'tab5',});
     }
   }

  renderTabs() {
     switch (this.state.activeTab) {
       case 'uppend':
         return <Tab1 />
         break
       case 'mypend':
         return <Tab2 />
         break
       case 'upload':
         return <Tab3 />
         break
       case 'mypurch':
         return <Tab4 />
         break
       case 'copurch':
         return <Tab5 />
         break

     }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1, marginTop:25 }}>
          {this.renderTabs()}
        </View>

        <BottomNavigation
          tabs={this.tabs}
          activeTab={this.state.activeTab}
          onTabPress={newTab => this.setState({ activeTab: newTab.key })}
          renderTab={this.renderTab}
          useLayoutAnimation
          onTabChange={this.handleTabChange}
        />
      </View>
    )
  }
}

export default Home;
