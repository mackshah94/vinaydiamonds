import React from 'react';
import {Text, View, Image, ScrollView, Picker} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import axios from 'axios';
import {Card, Button, BlueCardSection, RoundedButton} from '../components/common';
import {mainurl} from '../loginApi';
import Icon from "react-native-vector-icons/FontAwesome";

export default class SinglePending extends React.Component {

    state = {reasons: [], selectedReason: '', selectedStatus: 'Complete'};

    constructor() {
        super();

        this.onSelect = this.onSelect.bind(this)
    }

    componentDidMount() {
        axios.get(mainurl + 'getCancelReasons').then(response => {
                this.setState({reasons: response.data.cancel});
            }
        );
    }

    onSelect(index, value) {
        this.setState({
            selectedStatus: value
        })
    }

    renderReasons() {
        console.log(this.state.reasons);
        return this.state.reasons.map(reason =>
            <Picker.Item label={reason.description} value={reason.id} key={reason.id} color='red' />
        );
    }

    renderSpinner() {
        if (this.state.selectedStatus === 'Cancel') {
            return (
                <BlueCardSection style={{backgroundColor: '#181c28',flexDirection: 'column'}}>
                    <Text style={styles.textStyle}>Reason</Text>
                    <Picker
                        style={{
                            flex: 1,
                            justifyContent: 'flex-start',
                            backgroundColor: '#292d39',
                            borderRadius: 15,
                            marginLeft: 10,
                            marginRight: 10,
                        }}
                        selectedValue={this.state.selectedReason}
                        onValueChange={(itemValue, itemIndex) => this.setState({selectedReason: itemValue})}>

                        {this.renderReasons()}
                    </Picker>
                </BlueCardSection>
            );
        } else {
            <Text></Text>
        }
    }

    submitResponse() {
        const data = new FormData();

        data.append('id', this.props.mainid);
        data.append('status', this.state.selectedStatus);
        data.append('cancelreason', this.state.selectedReason || '');

        axios.post(mainurl + 'updateStatus', data);

        Actions.main();
    }


    render() {
        return (

            <ScrollView>
                <Card>

                    <BlueCardSection style={{backgroundColor: '#181c28'}}>
                        <View>
                            <Image
                                style={styles.imageStyle}
                                source={{uri: this.props.photo}}
                            />
                        </View>
                    </BlueCardSection>

                    <BlueCardSection style={{backgroundColor: '#181c28', justifyContent: 'center'}}>

                        <Text style={styles.certiStyle}>
                            {this.props.certinumber}
                        </Text>

                    </BlueCardSection>

                    <BlueCardSection style={{backgroundColor: '#181c28', justifyContent: 'center'}}>
                        <View style={styles.dateContainer}>
                            <Icon size={16} color='#fff' name={'calendar'}/>
                            <Text style={styles.textStyle}>
                                - {this.props.d_date}
                            </Text>
                        </View>
                    </BlueCardSection>

                    <BlueCardSection style={{backgroundColor: '#181c28'}}>
                        <View style={styles.viewStyle}>
                            <Text style={styles.textStyle}>
                                Check for: {this.props.checkfor}
                            </Text>
                        </View>
                    </BlueCardSection>

                    <View
                        style={{
                            borderBottomColor: '#f4f4f4',
                            borderBottomWidth: 1,
                            marginBottom: 5
                        }}
                    />

                    <BlueCardSection style={{backgroundColor: '#181c28'}}>
                        <Text style={styles.textStyle}>
                            Remarks: {this.props.remarks}
                        </Text>
                    </BlueCardSection>

                    <BlueCardSection style={{backgroundColor: '#181c28'}}>
                        <Text style={styles.textStyle}>
                            Query By: {this.props.name}
                        </Text>
                    </BlueCardSection>

                    <BlueCardSection style={{backgroundColor: '#181c28', flexDirection: 'column'}}>
                        <RadioGroup
                            onSelect={(index, value) => this.onSelect(index, value)}
                        >
                            <RadioButton value={'Complete'}>
                                <Text style={styles.textStyle}>Complete</Text>
                            </RadioButton>

                            <RadioButton value={'Cancel'}>
                                <Text style={styles.textStyle}>Cancel</Text>
                            </RadioButton>

                        </RadioGroup>
                    </BlueCardSection>

                    <View
                        style={{
                            borderBottomColor: 'white',
                            borderBottomWidth: 1,
                            marginBottom: 5
                        }}
                    />

                    <View>
                        {this.renderSpinner()}
                    </View>

                    <BlueCardSection style={{backgroundColor: '#181c28'}}>
                        <RoundedButton onPress={() => this.submitResponse()}>
                            Submit
                        </RoundedButton>
                    </BlueCardSection>
                </Card>
            </ScrollView>
        );
    }
}

const styles = {
    imageStyle: {
        height: 200,
        width: 300,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    viewStyle: {
        flex: 1,
        width: null,
        flexDirection: 'column'
    },
    textStyle: {
        fontSize: 18,
        color: '#fff'
    },
    certiStyle: {
        fontSize: 20,
        color: '#007aff',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
    container: {
        marginTop: 5,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        padding: 10,
        fontSize: 14,
    },
    dateContainer: {
        flex: 1,
        backgroundColor: '#181c28',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'

    },
};
