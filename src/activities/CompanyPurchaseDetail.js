import React,{ Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import {Card,BlueCardSection,Button} from '../components/common';
import Icon from "react-native-vector-icons/FontAwesome";

const CompanyPurchaseDetail = ({detail}) => {

    const { photo,d_date,certinumber,checkfor,remarks,name} = detail;

    return (
        <View style={styles.cardContainer}>


            <View style={styles.dateContainer}>
                <Icon size={16} color='#fff' name={'calendar'}/>
                <Text style={styles.dateStyle}>{d_date}</Text>
            </View>

            <BlueCardSection>

                <View style={styles.thumbnailContainer}>
                    <Image style={styles.thumbnailStyle}
                           source={{uri: photo}}
                    />
                </View>
                <Text style={styles.certiStyle}>{certinumber}</Text>

            </BlueCardSection>

            <Text style={[styles.textStyle,styles.marginStyle]}>{checkfor}</Text>

            <View
                style={{
                    borderBottomColor: 'white',
                    borderBottomWidth: 1,
                    marginBottom: 5
                }}
            />


            <BlueCardSection>
                <Text style={styles.remarkStyle}> {remarks} </Text>
            </BlueCardSection>

            <BlueCardSection>
                <Text style={{color:'#fff',marginTop:5}}>Purchased By: </Text>
                <Text style={styles.nameStyle}>{name}</Text>
            </BlueCardSection>
        </View>
    );
};

const styles = {
    container: {
        flex: 1,
        backgroundColor:'#181c28'
    },
    headerDetails: {
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    thumbnailStyle: {
        height: 75,
        width: 75
    },
    thumbnailContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerStyle: {
        fontSize: 20
    },
    certiStyle: {
        flex: 1,
        width: null,
        color: '#0295ff',
        marginTop: 20,
        fontSize: 24,
        fontWeight: '600'
    },
    textStyle: {
        flex: 1,
        width: null,
        color: '#fff'
    },
    remarkStyle: {
        flex: 1,
        width: null,
        color: '#fff',
        fontSize: 20
    },
    nameStyle: {
        color: '#fff',
        fontSize: 20
    },
    dateStyle: {
        width: null,
        flexDirection: 'row',
        alignItems:'flex-end',
        paddingTop: 10,
        paddingRight: 20,
        marginLeft:10,
        color: '#fff'
    },
    dateContainer:{
        flex: 1,
        backgroundColor: '#292d39',
        alignItems:'flex-end',
        flexDirection:'row',
        justifyContent:'flex-end'

    },
    cardContainer:{
        borderRadius: 15,
        marginLeft:5,
        marginRight:5,
        marginTop:10,
        padding: 4,
        backgroundColor:'#292d39'
    },
    marginStyle:{
        marginLeft: 100
    }
};

export default CompanyPurchaseDetail;
