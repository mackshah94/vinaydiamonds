import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    ScrollView,
    Picker,
    TextInput,
    TouchableOpacity,
    Image,
    PixelRatio
} from 'react-native';
import axios from 'axios';
import {
    MenuContext,
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    MenuProvider
} from 'react-native-popup-menu';
import ImagePicker from 'react-native-image-picker';
import {
    BlueCardSection,
    Button,
    Card,
    CardSection,
    Input,
    RoundedButton,
    RoundedInput,
    Spinner
} from '../components/common';
import {mainurl} from '../loginApi';
import {Actions} from "react-native-router-flux";
import Toast from 'react-native-simple-toast';

export default class Tab3 extends React.Component {

    state = {
        certino: '',
        loading: false,
        error: '',
        reasons: [],
        users: [],
        selectedUser: '',
        selectedReason: '',
        avatarSource: null,
        remark: '',
        fromUser: ''
    };


    componentDidMount() {
        axios.get(mainurl + 'getUploadDetails').then(response => {
                this.setState({reasons: response.data.reasons, users: response.data.users});
            }
        );

        AsyncStorage.getItem('id').then((token) => {
            if (token !== null)
                this.setState({fromUser: token});

        });
    }


    onButtonPress() {
        const data = new FormData();

        data.append('from', this.state.fromUser);
        data.append('to', this.state.selectedUser);
        data.append('certi', this.state.certino);
        data.append('remarks', this.state.remark);
        data.append('checkfor', this.state.selectedReason);
        data.append('photo[]', {
            uri: this.state.avatarSource,
            type: 'image/jpeg',
            name: 'testPhotoName'
        });


        axios.post(mainurl + 'addData', data)
            .then(response => this.onUploadSuccess(response.data))
            .catch(() => this.onUploadFail());

        this.setState({error: '', loading: true});

    }


    onUploadSuccess(dataa) {

        if (dataa.error) {
            this.setState({loading: false, error: 'Sorry! Image could not be uploaded on server'});
        } else {
            this.setState({error: '', certino: '', remark: '', avatarSource: null});
            Toast.showWithGravity('Data uploaded successfully', Toast.LONG, Toast.TOP);
            setTimeout(() => {
                this.setState({loading: false});
                Actions.tab1();
            }, 1000)
        }
    }

    onUploadFail() {
        this.setState({loading: false, error: 'Sorry! Image could not be uploaded'});
    }

    renderReasons() {
        return this.state.reasons.map(reason =>
            <Picker.Item label={reason.description} value={reason.id} key={reason.id} color='#04ff06'/>
        );
    }

    renderUsers() {
        return this.state.users.map(user =>
            <Picker.Item label={user.name} value={user.id} key={user.id} color='#04ff06'/>
        );
    }

    renderButton() {
        if (this.state.loading) {
            return (
                <Spinner size='small'/>
            );
        } else {
            return (
                <RoundedButton onPress={this.onButtonPress.bind(this)}>
                    Upload
                </RoundedButton>
            );
        }
    }

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = {uri: response.uri};

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source
                });
            }
        });
    }


    render() {

        return (
            <ScrollView style={{backgroundColor: '#181c28', flex: 1, }}>

                <View style={[styles.borderStyle]}>

                    <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                        <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                            {this.state.avatarSource === null ? <Text style={{color: '#fff'}}>Select a Photo</Text> :
                                <Image style={styles.avatar} source={this.state.avatarSource}/>
                            }
                        </View>
                    </TouchableOpacity>

                </View>


                <RoundedInput
                    placeholder="Certificate Number"
                    value={this.state.certino}
                    onChangeText={text => this.setState({certino: text})}
                />


                <BlueCardSection style={{flexDirection: 'column'}}>

                    <Text style={styles.pickerTextStyle}>To:</Text>
                    <Picker
                        style={{flex: 1, justifyContent: 'flex-start'}}
                        selectedValue={this.state.selectedUser}
                        onValueChange={(itemValue, itemIndex) => this.setState({selectedUser: itemValue})}>


                        {this.renderUsers()}
                    </Picker>
                </BlueCardSection>

                <BlueCardSection style={{flexDirection: 'column'}}>

                    <Text style={styles.pickerTextStyle}>Select Reason:</Text>
                    <Picker
                        style={{flex: 1, justifyContent: 'flex-start'}}
                        selectedValue={this.state.selectedReason}
                        onValueChange={(itemValue, itemIndex) => this.setState({selectedReason: itemValue})}>

                        {this.renderReasons()}
                    </Picker>

                </BlueCardSection>

                <View style={styles.cardContainer}>
                    <View style={styles.textAreaContainer}>
                        <TextInput

                            style={styles.textArea}
                            underlineColorAndroid="transparent"
                            placeholder="Remarks(Optional)"
                            numberOfLines={4}
                            multiline={true}
                            value={this.state.remark}
                            onChangeText={text => this.setState({remark: text})}
                        />

                    </View>
                </View>

                <Text style={styles.errorStyle}>
                    {this.state.error}
                </Text>

                <BlueCardSection>
                    {this.renderButton()}
                </BlueCardSection>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    textAreaContainer: {
        borderColor: '#333333',
        borderWidth: 1,
        borderRadius: 3,
        padding: 5
    },
    textArea: {
        height: 100
    },
    errorStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    avatarContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        borderRadius: 5,
        height: 100,
        margin: 5,
        width: 350,
    },
    borderStyle: {
        height: 100,
        marginRight: 5,
        marginLeft: 5,
        padding: 5,
        flex: 1
    },
    cardContainer: {
        marginBottom: 5,
        padding: 5,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative'
    },
    pickerTextStyle: {
        fontSize: 18,
        paddingLeft: 20,
        color: '#fff'
    }
});
