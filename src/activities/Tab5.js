import React from 'react';
import {Text, View, AsyncStorage, TouchableOpacity, Image} from 'react-native';
import { Actions } from 'react-native-router-flux';

export default class Tab5 extends React.Component {

    renderLogout(){
        AsyncStorage.removeItem('id').then(() => {
            Actions.pop();
            Actions.auth();
        });

    }
  render() {
    return (
      <View style={{ flex:1, flexDirection: 'column',backgroundColor: '#181c28'}}>

          <View style={styles.logoContainer}>
              <Image style={styles.logo} resizeMode={'contain'} source={require('../icons/icon.png')}/>
          </View>

          <TouchableOpacity style={styles.buttonStyle} onPress={() => Actions.companyPurchase()}>
              <Text style={styles.textStyle}>Company Purchases</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonStyle} onPress={() => Actions.allData()}>
              <Text style={styles.textStyle}>Search All Data</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.buttonStyle} onPress={() => this.renderLogout()}>
              <Text style={styles.textStyle}>Log Out</Text>
          </TouchableOpacity>

      </View>
    );
  }
}

const styles = {
    buttonStyle:{
        alignSelf: 'stretch',
        backgroundColor:'#292d39',
        borderRadius:25,
        borderWidth:1,
        borderColor:'#292d39',
        marginLeft:15,
        marginRight:15,
        marginTop: 20,
        height: 50
    },
    textStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'#fff',
        fontWeight:'600',
        paddingTop:10,
        paddingBottom:10
    },
    logoContainer:{
        alignItems:'center',
        marginTop: 75,
        marginBottom: 75
    },
    logo:{
        width:'70%'
    },
};
