import React from 'react';
import {Image, StyleSheet, Text, View, AsyncStorage, FlatList, Modal} from 'react-native';
import {mainurl, imageurl} from '../loginApi';
import axios from 'axios';
import {Card, CardSection, Button, Dialog, Alert, Spinner, BlueCardSection} from '../components/common';
import Icon from "react-native-vector-icons/FontAwesome";

export default class Tab1 extends React.Component {

    state = {details: [], showDialog: false, loading: true, nodata: ''};

    componentDidMount() {
        AsyncStorage.getItem('id').then((token) => {
            console.log(token);
            axios.get(mainurl + 'getMyUploadedPendings/' + token).then(response => {
                    if (response.data.pendings !== null) {
                        this.setState({details: response.data.pendings, loading: false, nodata: ''});
                    } else {
                        this.setState({nodata: 'none', loading: false});
                    }
                }
            );
        });
    }

    onAccept(key) {
        axios.get(mainurl + 'deletePurchase/' + key).then(() => {
                this.setState({showDialog: false});
                Alert.alert(
                    'Data Deleted Successfully',
                    '',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    {cancelable: false}
                );
            }
        );

    }

    onDecline() {
        this.setState({showDialog: false});
    }

    renderDetails() {

        if (this.state.nodata === 'none') {
            return (
                <View style={styles.fetchingData}>
                    <Text style={styles.textStyle}>Sorry, No Data Available...</Text>
                </View>
            );
        } else {
            if (this.state.loading) {
                return (
                    <View style={styles.fetchingData}>
                        <Spinner size='large'/>
                        <Text style={styles.textStyle}>Loading...</Text>
                    </View>
                );
            } else {
                return this.state.details.map(detail =>

                    <View style={styles.cardContainer} key={detail.mainid}>


                        <View style={styles.dateContainer}>
                            <Icon size={16} color='#fff' name={'calendar'}/>
                            <Text style={styles.dateStyle}>{detail.d_date}</Text>
                        </View>

                        <BlueCardSection>

                            <View style={styles.thumbnailContainer}>
                                <Image style={styles.thumbnailStyle}
                                       source={{uri: detail.photo}}
                                />
                            </View>
                            <Text style={styles.certiStyle}>{detail.certinumber}</Text>

                        </BlueCardSection>

                        <Text style={[styles.textStyle,styles.marginStyle]}>{detail.checkfor}</Text>

                        <View
                            style={{
                                borderBottomColor: 'white',
                                borderBottomWidth: 1,
                                marginBottom: 5
                            }}
                        />


                        <BlueCardSection>
                            <Text style={styles.remarkStyle}> {detail.remarks} </Text>
                        </BlueCardSection>

                        <BlueCardSection>

                            <Button onPress={() => this.setState({showDialog: !this.state.showDialog})}>
                                Delete?
                            </Button>

                            <Dialog
                                visible={this.state.showDialog}
                                onAccept={this.onAccept.bind(this, detail.mainid)}
                                onDecline={this.onDecline.bind(this)}
                            >
                                Are you sure you want to delete this entry?
                            </Dialog>

                        </BlueCardSection>
                    </View>
                );
            }
        }
    }

    render() {
        return (
            <FlatList style={styles.container}>
                {this.renderDetails()}
            </FlatList>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#181c28',
    },
    headerDetails: {
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    thumbnailStyle: {
        height: 75,
        width: 75
    },
    thumbnailContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerStyle: {
        fontSize: 20
    },
    certiStyle: {
        flex: 1,
        width: null,
        color: '#0295ff',
        marginTop: 20,
        fontSize: 24,
        fontWeight: '600'
    },
    textStyle: {
        flex: 1,
        width: null,
        color: '#fff'
    },
    remarkStyle: {
        flex: 1,
        width: null,
        color: '#fff',
        fontSize: 20
    },
    nameStyle: {
        color: '#fff',
        fontSize: 20
    },
    dateStyle: {
        width: null,
        flexDirection: 'row',
        alignItems:'flex-end',
        paddingTop: 10,
        paddingRight: 20,
        marginLeft:10,
        color: '#fff'
    },
    dateContainer:{
        flex: 1,
        backgroundColor: '#292d39',
        alignItems:'flex-end',
        flexDirection:'row',
        justifyContent:'flex-end'

    },
    cardContainer:{
        borderRadius: 15,
        marginLeft:5,
        marginRight:5,
        marginTop:10,
        padding: 4,
        backgroundColor:'#292d39'
    },
    marginStyle:{
        marginLeft: 100
    },
    fetchingData: {
        justifyContent: 'center',
        alignItems: 'center'
    },

};
