import React, {Component} from 'react';
import {Text, View, Image, Header, ListView, FlatList} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';
import SearchInput, {createFilter} from 'react-native-search-filter';
import {BlueCardSection, Spinner} from '../components/common';
import {mainurl} from '../loginApi';

const KEYS_TO_FILTERS = ['certinumber'];
let lim = 5;
let offset = 0;
let loadornot = false;
let lastdata = 0;

export default class AllData extends Component {

    constructor(props) {
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {details: [], initial: [], loading: true, nodata: '', searchTerm: '', isMoreData: false};
        lim = 5;
        offset = 0;
        loadornot = false;
        lastdata = 0;

        this.arrayholder = [];
        this.onEndReachedCalledDuringMomentum = true;
    }

    searchUpdated(term) {
        this.setState({searchTerm: term})
    }

    componentWillMount() {

        this.apiCall();
    }

    apiCall() {
        console.log('called ' + lim + ' ' + offset + ' ' + lastdata);
        if (lastdata === offset) {
            axios.get(mainurl + 'getAllData/' + lim + '/' + offset).then(response => {
                    console.log(response.data.alldata.length);
                    lastdata = response.data.alldata.length;
                    if (response.data.alldata.length > 0) {
                        if (response.data.alldata !== null) {
                            var newdata = this.state.details.concat(response.data.alldata);
                            this.setState({details: newdata, initial: newdata, loading: false, nodata: ''});

                            this.arrayholder = newdata;
                        } else {
                            this.setState({nodata: 'none', loading: false});
                        }
                    } else {
                        this.setState({details: this.state.details, initial: this.state.details, loading: false, nodata: ''});
                    }
                }
            );
        } else{
            console.log('else')
        }
    }

    SearchFilterFunction(e) {

        let textt = e.toLowerCase();
        let datas = this.state.initial;
        let filteredName = datas.filter((item) => {
            if(item.certinumber.toLowerCase().match(textt))
                return item;
        });

        if (!textt || textt === '') {
            this.setState({
                details: this.state.initial
            })
        } else if (!filteredName.length) {
            // set no data flag to true so as to render flatlist conditionally
            this.setState({
                nodata: true
            })
        } else if (Array.isArray(filteredName)) {
            this.setState({
                nodata: false,
                details: filteredName
            })
        }

        /*console.log(text + '  ' );
        const newData = this.arrayholder.filter(function (item) {
            console.log(text + '  ' + item);
            const itemData = item.certinumber.toLowerCase();
            const textData = text.toLowerCase();
            return itemData.indexOf(textData) > -1
        });
        var newdata = this.state.details.concat(newData);
        this.setState({

            details: newdata,
            searchTerm: text
        })*/
    }

    renderDetails(item) {

        //const filteredEmails = this.state.details.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS));


        return (
            <View style={styles.cardContainer}>

                <View style={styles.dateContainer}>
                    <Icon size={16} color='#fff' name={'calendar'}/>
                    <Text style={styles.dateStyle}>{item.item.d_date}</Text>
                </View>

                <BlueCardSection>

                    <View style={styles.thumbnailContainer}>
                        <Image style={styles.thumbnailStyle}
                               source={{uri: item.item.photo}}
                        />
                    </View>
                    <Text style={styles.certiStyle}>{item.item.certinumber}</Text>

                </BlueCardSection>

                <BlueCardSection>
                    <Text style={[styles.textStyle]}>Check For: {item.item.checkreason}</Text>
                </BlueCardSection>

                <View
                    style={{
                        borderBottomColor: 'white',
                        borderBottomWidth: 1,
                        marginBottom: 5
                    }}
                />

                <BlueCardSection>
                    <Text style={styles.remarkStyle}> {item.item.remarks} </Text>
                </BlueCardSection>

                <BlueCardSection>
                    <Text style={styles.textStyle}>From: {item.item.from}</Text>
                    <Text style={styles.textStyle}>To: {item.item.to}</Text>
                </BlueCardSection>


                {(() => {
                    const stats = item.item.status;

                    if (stats === 'Complete') {

                        return (
                            <View style={styles.statusContainer}>
                                <Text style={styles.statustextStyle}>Status: {item.item.status}</Text>
                            </View>
                        );
                    } else if (stats === 'Cancel') {
                        return (
                            <View style={styles.statusContainer}>
                                <Text style={styles.statuscanceltextStyle}>Cancellation
                                    Reason: {item.item.creason}</Text>
                            </View>
                        );
                    }
                })()}


            </View>
        );
    }


    render() {

        if (this.state.nodata === 'none') {
            return (
                <View style={styles.fetchingData}>
                    <Text style={styles.textStyle}>Sorry, No Data Available...</Text>
                </View>
            );
        } else if (this.state.loading) {
            return (
                <View style={styles.fetchingData}>
                    <Spinner size='large'/>
                    <Text style={styles.textStyle}>Loading...</Text>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <SearchInput
                        onChangeText={(text) => this.SearchFilterFunction(text)}
                        style={styles.searchInput}
                        placeholder="Type to search..."
                        placeholderTextColor='rgb(255,255,255)'

                    />

                    <FlatList
                        data={this.state.details}
                        extraData={this.state.details}
                        renderItem={this.renderDetails}
                        keyExtractor={(item, index) => index.toString()}

                        onEndReached={() => {
                            //this.setState({loading: true});
                            offset += 5;
                            this.apiCall();
                            /*if (!this.onEndReachedCalledDuringMomentum) {

                                this.onEndReachedCalledDuringMomentum = true;

                            }*/
                        }}
                        onEndReachedThreshold={0.2}

                    >

                    </FlatList>
                </View>
            );
        }
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#181c28'
    },
    headerDetails: {
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    thumbnailStyle: {
        height: 75,
        width: 75
    },
    thumbnailContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    headerStyle: {
        fontSize: 20
    },
    certiStyle: {
        flex: 1,
        width: null,
        color: '#0295ff',
        marginTop: 20,
        fontSize: 24,
        fontWeight: '600'
    },
    textStyle: {
        flex: 1,
        width: null,
        color: '#fff'
    },
    remarkStyle: {
        flex: 1,
        width: null,
        color: '#fff',
        fontSize: 20
    },
    fetchingData: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    searchInput: {
        padding: 10,
        borderColor: '#CCC',
        borderBottomWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        color: '#fff'
    },
    searchContainer: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'flex-start'
    },
    dateStyle: {
        width: null,
        flexDirection: 'row',
        alignItems: 'flex-end',
        paddingTop: 10,
        paddingRight: 20,
        marginLeft: 10,
        color: '#fff'
    },
    dateContainer: {
        flex: 1,
        backgroundColor: '#292d39',
        alignItems: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'flex-end'

    },
    cardContainer: {
        borderRadius: 15,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 4,
        backgroundColor: '#292d39'
    },
    statusContainer: {
        marginBottom: 5,
        padding: 5,
        backgroundColor: '#292d39',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        position: 'relative'
    },
    statustextStyle: {
        flex: 1,
        color: '#00ff00',
        textAlign: 'center',
    },
    statuscanceltextStyle: {
        flex: 1,
        color: '#f00',
        textAlign: 'center',
    },
};

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {

    if (loadornot) {
        return false;
    } else {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    }
};