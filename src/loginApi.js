import {HttpClient} from './httpClient'

export const mainurl = 'http://theuntitleddevelopers.com/vinaydiamonds/v1/';
export const imageurl = 'http://theuntitleddevelopers.com/vinaydiamonds/admin/uploads/';

const LOGIN_API = `${HttpClient.auth_base_url}login/`
const SIGNUP_API = `${HttpClient.base_url}create_user/`
const PROFILE_API = `${HttpClient.base_url}user_profile/`

export const login = (params, config) => {
  return HttpClient.post(LOGIN_API, params, config)
}

export const signUp = (params, config) => {
  return HttpClient.post(SIGNUP_API, params, config)
}

export const profile = (config) => {
  return HttpClient.get(PROFILE_API, {},{ headers: config })
}

export const editProfile = (params,config) => {
  return HttpClient.put(PROFILE_API,params,{ headers: config })
}
