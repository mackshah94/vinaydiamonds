import React from 'react';
import {StyleSheet, Text, ScrollView, Image, View, AsyncStorage, Alert} from 'react-native';
import axios from 'axios';
import {RoundedInput, Spinner, RoundedButton} from './components/common';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import {Actions} from 'react-native-router-flux';
import {mainurl} from './loginApi';
import firebase, {Notification} from 'react-native-firebase';

class LoginComponent extends React.Component {


    state = {email: '', password: '', error: '', loading: false, logindata: []};

    async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }


    onButtonPress() {

        this.setState({error: '', loading: true});

        axios.get(mainurl + 'login/' + this.state.email + '/' + this.state.password)
            .then(response => this.onLoginSuccess(response.data))
            .catch(response => this.onLoginFail(response.data));

    }

    onLoginSuccess(userdata) {
        console.log('success ' + userdata);
        this.saveItem('id', userdata.id.toString());
        this.saveItem('name', userdata.fullname.toString());
        this.saveItem('username', this.state.email.toString());
        this.saveItem('upcnt',userdata.upcnt.toString());
        this.saveItem('mycnt',userdata.mycnt.toString());
        this.setState({
            email: '',
            password: '',
            loading: false,
            error: ''
        });

        Actions.main();
    }

    onLoginFail(data) {
        console.log(data);
        this.setState({
            loading: false,
            error: 'Authentication Failed!'
        });
    }

    renderButton() {
        if (this.state.loading) {
            return (
                <Spinner size='small'/>
            );
        } else {
            return (
                <RoundedButton onPress={this.onButtonPress.bind(this)}>
                    Login
                </RoundedButton>
            );
        }
    }

    render() {
        return (

            <ScrollView>
                <View style={styles.mainContainer}>

                    <View style={styles.logoContainer}>
                        <Image style={styles.logo} resizeMode={'contain'} source={require('./icons/icon.png')}/>
                    </View>

                    <View style={styles.SimpleContainer}>
                        <RoundedInput
                            placeholder="Username"
                            value={this.state.email}
                            onChangeText={text => this.setState({email: text})}
                        />
                    </View>

                    <View style={styles.SimpleContainer}>
                        <RoundedInput
                            placeholder="Password"
                            value={this.state.password}
                            secureTextEntry
                            onChangeText={text => this.setState({password: text})}
                        />
                    </View>

                    <Text style={styles.errorStyle}>
                        {this.state.error}
                    </Text>

                    <View style={[styles.SimpleContainer, styles.marginStyle]}>
                        {this.renderButton()}
                    </View>

                </View>
            </ScrollView>
        );
    }
}

const styles = {
    errorStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    },
    mainContainer: {
        flex: 1,
        backgroundColor: '#181c28',
        marginTop: 25
    },
    logoContainer: {
        alignItems: 'center',
        marginTop: 75,
        marginBottom: 75
    },
    logo: {
        width: '70%'
    },
    inputContainer: {
        flex: 3,
        padding: 20
    },
    link: {
        textDecorationLine: 'underline',
        textAlign: 'center'
    },
    SimpleContainer: {
        padding: 5,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        marginTop: 20
    },
    marginStyle: {
        marginTop: 50
    }
};

export default LoginComponent;
