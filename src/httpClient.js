import axios from 'axios'
const base = 'http://192.168.1.106:81/vinaydiamonds/v1/'
//const base = 'http://83aa80c2.ngrok.io/'
//const image_base = 'http://178.128.49.14'
const image_base = 'http://192.168.1.106:81/vinaydiamonds/admin/uploads'
const auth_base_url = base +'rest-auth/';
const base_url = base +'srs/';

const post = (url = '', data = '', config = {}) => {
  console.log("calling POST api------"+url)
  return axios.post(url, data, config)
}

const get = (url='', data='', config={}) => {
  config.params = data;
  config.method = "get";
  console.log("calling GET api------"+url)
  return axios.get(url,config)
}

const put = (url = '', data = '', config = {}) => {
  console.log("calling PUT api------"+url)
  return axios.put(url, data, config)
}

const del = (url = '', config = {}) => {
  console.log("calling DEL api------"+url)
  return axios.delete(url, config)
}

const HttpClient = {
  post,
  get,
  put,
  delete: del,
  auth_base_url,
  base_url,
  base,
  image_base
}

export {HttpClient}
