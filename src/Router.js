import React from 'react';
import {Scene,Router} from 'react-native-router-flux';
import LoginComponent from './LoginComponent';
import Home from './activities/Home';
import SinglePending from './activities/SinglePending';
import CompanyPurchases from './activities/CompanyPurchases';
import AllData from './activities/AllData';
import Tab1 from './activities/Tab1';

const RouterComponent = (props) =>{
  console.log(props);
  return(

    <Router>
      <Scene key = "root" >

        <Scene key = "auth" hideNavBar initial= {!props.logged} panHandlers={null}>
          <Scene key="login"
            component={LoginComponent}
            title="Vinay Diamonds"
             />
        </Scene>

        <Scene key = "main" hideNavBar initial= {props.logged} panHandlers={null}>
          <Scene
            rightTitle="Search"
            key="homePage"
            component={Home}
            title="Home"
             />
        </Scene>

        <Scene key= "single"
          title=""
          component={SinglePending}>
        </Scene>

        <Scene
          key = "companyPurchase"
          title = "Company Purchases"
          component = {CompanyPurchases}>
        </Scene>

        <Scene
          key = "allData"
          title = "Search All Data"
          component = {AllData}>
        </Scene>

        <Scene
            key = "tab1"
            component = {Tab1}>
        </Scene>
      </Scene>
    </Router>
  );
};

export default RouterComponent;
