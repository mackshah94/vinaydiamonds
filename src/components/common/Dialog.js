import React from 'react';
import {Text, View, Modal} from 'react-native';
import {CardSection} from './CardSection';
import {Button} from './Button';

const Dialog = ({children, onDecline, onAccept, visible}) => {
    return (
        <Modal
            animationType="slide"
            onRequestClose={() => {
            }}
            transparent
            visible={visible}
        >
            <View style={styles.containerStyle}>
                <CardSection style={styles.cardSecStyle}>
                    <Text style={styles.textStyle}>
                        {children}
                    </Text>
                </CardSection>

                <CardSection>
                    <Button onPress={onAccept}>Yes</Button>
                    <Button onPress={onDecline}>No</Button>
                </CardSection>
            </View>
        </Modal>
    );
};

const styles = {
    cardSecStyle: {
        justifyContent: 'center',
        marginLeft: 20,
        marginRight: 20,
        borderRadius:10

    },
    textStyle: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40
    },
    containerStyle: {
        backgroundColor: 'rgba(0,0,0,0.75)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    }
};
export {Dialog};
