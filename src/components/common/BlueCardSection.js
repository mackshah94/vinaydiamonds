import React  from 'react';
import { View } from 'react-native';


const BlueCardSection = (props) => {
    return (
        <View style={[styles.container,props.style]}>
            {props.children}
        </View>
    );
};

const styles = {
    container:{
        marginBottom:5,
        padding:5,
        backgroundColor:'#292d39',
        justifyContent:'flex-start',
        flexDirection:'row',
        position:'relative'
    }
}
export {BlueCardSection};
