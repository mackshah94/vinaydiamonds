import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const RoundedButton = ({onPress,children}) =>{
    return (
        <TouchableOpacity style={styles.buttonStyle} onPress={onPress}>
            <Text style={styles.textStyle}>{children}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonStyle:{
        flex:1,
        alignSelf: 'stretch',
        backgroundColor:'#0295ff',
        borderRadius:30,
        borderWidth:1,
        borderColor:'#0295ff',
        marginLeft:15,
        marginRight:15,
        height: 60
    },
    textStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'#fff',
        fontWeight:'700',
        paddingTop:15,
        paddingBottom:20,

    }
};
export {RoundedButton} ;
