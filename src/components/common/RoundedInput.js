import React from 'react';
import { TextInput, View } from 'react-native';


const RoundedInput = ({value, onChangeText, placeholder, secureTextEntry}) =>{
    return(
        <View style={styles.containerStyle}>

            <TextInput
                secureTextEntry={secureTextEntry}
                style={styles.inputStyle}
                value = {value}
                onChangeText = {onChangeText}
                autoCorrect={false}
                placeholder={placeholder}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.5)'
            />
        </View>
    )};

const styles ={
    inputStyle:{
        color:'#fff',
        paddingRight:5,
        paddingLeft:5,
        fontSize:22,

    },

    containerStyle:{
        height:60,
        flex:1,
        paddingLeft:15,
        paddingRight:15,
        borderRadius:30,
        marginLeft:15,
        marginRight:15,
        borderColor: '#292d39',
        backgroundColor: '#292d39',
        justifyContent:'center'
    }
};

export {RoundedInput};
