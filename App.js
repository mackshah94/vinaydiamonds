import React from 'react';
import { AsyncStorage } from 'react-native';
import Router from './src/Router';
import {registerAppListener} from "./src/Listeners";
import firebase from 'react-native-firebase';

export default class App extends React.Component {

    state = { loggedin: 'false',token: '', tokenCopyFeedback: '' };

    componentWillMount() {

        AsyncStorage.getItem('id').then((token) => {
            if (token !== null)
                this.setState({ loggedin: true });
            else
                this.setState({ loggedin: false });

        });
    }

    async componentDidMount(){

/*
        registerAppListener(this.props.navigation);

        firebase.notifications().getInitialNotification()
            .then((notificationOpen: NotificationOpen) => {
                if (notificationOpen) {
                    // Get information about the notification that was opened
                    const notif: Notification = notificationOpen.notification;
                    this.setState({
                        initNotif: notif.data
                    })
                    if(notif && notif.targetScreen === 'detail'){
                        setTimeout(()=>{
                            this.props.navigation.navigate('Detail')
                        }, 500)
                    }
                }
            });*/

        if (!await firebase.messaging().hasPermission()) {
            try {
                await firebase.messaging().requestPermission();
            } catch(e) {
                alert("Failed to grant permission")
            }
        }

        firebase.messaging().getToken().then(token => {
            console.log("TOKEN (getFCMToken)", token);
            this.setState({token: token || ""})
        });


        /*var offline = await AsyncStorage.getItem('headless')
        if(offline){
            this.setState({
                offlineNotif: offline
            });
            AsyncStorage.removeItem('headless');
        }*/
    }

    componentWillUnmount(){
        this.onTokenRefreshListener();
        this.notificationOpenedListener();
        this.messageListener();
    }



    render() {

        return (
            <Router logged={this.state.loggedin}/>
        );
    }
}
